package com.redeSocial.repositories;

import java.util.Optional;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.redeSocial.model.User;

@RunWith(SpringRunner.class)
@DataJpaTest
class UserRepositoryTest {

	@Autowired
	private UserRepository repository;
	
	@Test
	void testMethodFindByEmail() {
		String email = "rht@gmail.com";
		Optional<User> user = repository.findByEmail(email);
		Assert.assertNotNull(user);
	}

}
