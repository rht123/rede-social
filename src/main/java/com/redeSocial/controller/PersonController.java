package com.redeSocial.controller;

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.redeSocial.controller.dto.PersonDTO;
import com.redeSocial.model.Person;
import com.redeSocial.services.PersonService;

@RestController
@RequestMapping(value = "person")
public class PersonController {

	@Autowired
	private PersonService service;	
	
	
	@GetMapping
	@Cacheable(value = "listAll")
	public ResponseEntity<Page<PersonDTO>> findAll(@PageableDefault(sort="id", direction = Direction.ASC, page = 0, size = 10)Pageable page ) {
		Page<PersonDTO> list = service.findAll(page);
		return ResponseEntity.ok().body(list);
	}
	
	@GetMapping(value = "/{id}")
	public ResponseEntity<PersonDTO> findById(@PathVariable Long id) {
		Person obj = service.findById(id);
		PersonDTO objDTO = new PersonDTO(obj.getId(), obj.getName(), obj.getEmail(), obj.getCpf());
		
		return ResponseEntity.ok().body(objDTO);
	}
	
	@PostMapping
	@Transactional
	@CacheEvict(value = "listAll", allEntries = true)
	public ResponseEntity<PersonDTO> insert(@RequestBody Person obj) {
		obj = service.insert(obj);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getId()).toUri();
		return ResponseEntity.created(uri).body(new PersonDTO(obj));
	}
	
	@DeleteMapping(value = "/{id}")
	@Transactional
	@CacheEvict(value = "listAll", allEntries = true)
	public ResponseEntity<Void> delete(@PathVariable Long id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
	
	
	@PutMapping(value = "/{id}")
	@Transactional
	@CacheEvict(value = "listAll", allEntries = true)
	public ResponseEntity<PersonDTO> update(@PathVariable Long id, @RequestBody Person obj) {
		obj = service.update(id, obj);
		PersonDTO objDTO = new PersonDTO(obj);
		return ResponseEntity.ok().body(objDTO);
	}
}
	