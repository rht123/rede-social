package com.redeSocial.controller.dto;

import org.springframework.data.domain.Page;

import com.redeSocial.model.Person;

public class PersonDTO {

	private Long id;
	private String name;
	private String email;
	private String cpf;
	
	public PersonDTO(Long id, String name, String email, String cpf) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.cpf = cpf;
	}
	
	public PersonDTO(Person x) {
		id = x.getId();
		name = x.getName();
		email = x.getEmail();
		cpf = x.getEmail();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public static Page<PersonDTO> convert(Page<Person> person) {
		return person.map(PersonDTO::new);
	}
	
	
}
