package com.redeSocial.services.exception;

public class MissingParamaterException  extends RuntimeException {
	private static final long serialVersionUID = 1L;
	

	public MissingParamaterException(String msg) {
		super(msg);
	}
}
