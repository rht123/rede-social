package com.redeSocial.services;

import java.util.Optional;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.redeSocial.controller.dto.PersonDTO;
import com.redeSocial.model.Person;
import com.redeSocial.repositories.PersonRepository;
import com.redeSocial.services.exception.DatabaseException;
import com.redeSocial.services.exception.MissingParamaterException;
import com.redeSocial.services.exception.ResourceNotFoundException;

@Service
public class PersonService {

	@Autowired
	private PersonRepository repository;
	
	
	public Page<PersonDTO> findAll(Pageable page) {
		Page<Person> result =  repository.findAll(page);
		return PersonDTO.convert(result);
	}
	
	public Person findById(long id) {
		Optional<Person> obj = repository.findById(id);
		return obj.orElseThrow(() -> new ResourceNotFoundException(id));
	}

	public Person insert(Person obj) {
		try {
			return repository.save(obj);
		} catch(ConstraintViolationException e) {
			throw new MissingParamaterException(e.getMessage());
		}
	}

	public void delete(Long id) {
		try {
			repository.deleteById(id);
		} catch(EmptyResultDataAccessException e) {
			throw new ResourceNotFoundException(id);
		} catch(DataIntegrityViolationException e) {
			throw new DatabaseException(e.getMessage());
		}

	}
	
	public Person update(Long id, Person obj) {
		try {
			Person entity = repository.getById(id);
			updateData(entity, obj);
			return repository.save(entity);
		} catch (EntityNotFoundException e) {
			throw new ResourceNotFoundException(id);
		}

	}

	private void updateData(Person entity, Person obj) {
		entity.setName(obj.getName());
		entity.setEmail(obj.getEmail());
		entity.setPhone(obj.getPhone());
		entity.setCity(obj.getCity());
		entity.setState(obj.getCity());
		entity.setCpf(obj.getCpf());
	}
	
}
