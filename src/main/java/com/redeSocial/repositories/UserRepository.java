package com.redeSocial.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.redeSocial.model.User;

public interface UserRepository extends JpaRepository<User, Long>{

	Optional<User> findByEmail(String email);
}
