
package com.redeSocial.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.redeSocial.repositories.UserRepository;
import com.redeSocial.services.AuthenticationService;
import com.redeSocial.services.TokenService;

@EnableWebSecurity
@Configuration
@Profile("dev")
public class DevSecurityConfigurations extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private AuthenticationService service;

	@Autowired
	private TokenService tokenService;

	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		return super.authenticationManager();
	}

	/**
	 * Method to configure the authentication (access / login)
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(service).passwordEncoder(new BCryptPasswordEncoder());
	}

	/**
	 * Autorization config
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception {
//		http.authorizeRequests()
//				.antMatchers(HttpMethod.GET, "/person").permitAll()
//				.antMatchers(HttpMethod.GET, "/person/*").permitAll()
//				.antMatchers(HttpMethod.POST, "/auth").permitAll()
//				.anyRequest().authenticated().and().csrf().disable().sessionManagement()
//				.sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
//				.addFilterBefore(new AuthenticationTokenFilter(tokenService, userRepository),
//						UsernamePasswordAuthenticationFilter.class);

		http.authorizeRequests().antMatchers("/h2/**").permitAll()
        .and().csrf().ignoringAntMatchers("/h2/**")
        .and().headers().frameOptions().sameOrigin();
		
		http.authorizeRequests().
		antMatchers("/**").permitAll()
		.and().csrf().disable();
	}

	/**
	 * Static resources config (js, css, images, etc...)
	 */
	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**",
				"/swagger-resources/**");
	}

}
