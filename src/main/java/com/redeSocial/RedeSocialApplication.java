package com.redeSocial;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Profile;
import org.springframework.data.web.config.EnableSpringDataWebSupport;

import com.redeSocial.model.Person;
import com.redeSocial.model.User;
import com.redeSocial.repositories.PersonRepository;
import com.redeSocial.repositories.UserRepository;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSpringDataWebSupport
@EnableCaching
@EnableSwagger2
public class RedeSocialApplication implements ApplicationRunner {

	@Autowired
	private PersonRepository repository;
	
	@Autowired
	private UserRepository uRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(RedeSocialApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// TODO Auto-generated method stub
		uRepository.save(new User("Hisashi", "rht@gmail.com", "$2a$10$Wg6tdGMPIxkX5YLevimsU.CreGK8uCF6z32nP8IipPqaH0ih/O5wC"));
		repository.save(new Person("Rodrigo", "rodrigohisa@gmail.com", "456123", "456123", "sao paulo", "sao paulo"));
		repository.save(new Person("Renato", "renato@gmail.com", "456123", "456123", "sao paulo", "sao paulo"));
		repository.save(new Person("Cintia", "cintia@gmail.com", "456123", "456123", "sao paulo", "sao paulo"));
		repository.save(new Person("Carlos", "carlos@gmail.com", "456123", "456123", "sao paulo", "sao paulo"));
		repository.save(new Person("Roberto", "roberto@gmail.com", "456123", "456123", "sao paulo", "sao paulo"));
	}

}
